package com.application.erply.service;

import com.application.erply.model.Task;
import com.application.erply.repository.TaskRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class TaskService {

    @Autowired
    TaskRepository taskRepository;

    public List<Task> getAllTasks() {
        log.info("Get all the tasks.");
        return taskRepository.findAll();
    }

    public Task createTask(Task newTask) {
        log.info("Save task");
        return taskRepository.save(newTask);
    }

    public void deleteTask(Long id) {
        try {
            log.info("Delete task");
            taskRepository.deleteById(id);
        } catch (Exception ex) {
            log.info("Task with provided ID does not exist!");
        }
    }

    public Task updateTask(Task newTask, Long id) {
        log.info("Update task");
        return taskRepository.findById(id)
                .map(task -> {
                    task.setTaskName(newTask.getTaskName());
                    task.setDate(newTask.getDate());
                    task.setCompleted(newTask.isCompleted());
                    return taskRepository.save(task);
                })
                .orElseGet(() -> {
                    log.info("Save task under {}", id);
                    newTask.setId(id);
                    return taskRepository.save(newTask);
                });
    }

    public Task completeTask(Long id) {
        log.info("Complete task");
        return taskRepository.findById(id)
                .map(task -> {
                    task.setCompleted(true);
                    return taskRepository.save(task);
                })
                .orElseGet(() -> {
                    log.info("Task is not found");
                    return null;
                });
    }
}
