package com.application.erply.controller.api;

import com.application.erply.model.Task;
import com.application.erply.service.TaskService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "Task", description = "REST API for Task", tags = { "Task" })
public class TaskController {
    @Autowired
    private TaskService taskService;

    @GetMapping("/tasks")
    List<Task> getAll() {
        return taskService.getAllTasks();
    }

    @PostMapping("/addTask")
    Task newTask(@RequestBody Task newTask) {
        return taskService.createTask(newTask);
    }

    @PutMapping("/replaceTask/{id}")
    Task replaceTask(@RequestBody Task newTask, @PathVariable Long id) {
        return taskService.updateTask(newTask, id);
    }

    @PutMapping("/completeTask/{id}")
    Task completeTask(@PathVariable Long id) {
        return taskService.completeTask(id);
    }

    @DeleteMapping("/deleteTask/{id}")
    void deleteTask(@PathVariable Long id) {
        taskService.deleteTask(id);
    }
}
