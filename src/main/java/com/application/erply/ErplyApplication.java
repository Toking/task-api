package com.application.erply;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = {"com.application.erply.model"} )
@EnableJpaRepositories(basePackages = {"com.application.erply.repository"})
public class ErplyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ErplyApplication.class, args);
	}
}
