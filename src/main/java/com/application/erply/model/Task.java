package com.application.erply.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="tasks")
@NoArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "date")
    private Date date;

    @Column(name = "taskName")
    private String taskName;

    @Column(name = "completed")
    private boolean isCompleted;

    public Task(Date date, String taskName, boolean isCompleted) {
        this.date = date;
        this.taskName = taskName;
        this.isCompleted = isCompleted;
    }
}
