package com.application.erply.service;

import com.application.erply.model.Task;
import com.application.erply.repository.TaskRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class TaskServiceIntegrationTest {
	@TestConfiguration
	static class TaskServiceTestContextConfiguration {

		@Bean
		public TaskService taskService() {
			return new TaskService();
		}
	}

	@Autowired
	private TaskService taskService;

	@MockBean
	private TaskRepository taskRepository;

	@Before
	public void setUp() {
		Task task = new Task(new Date(), "First task", false);
		Task task2 = new Task(new Date(), "Second task", false);
		ArrayList<Task> tasks = new ArrayList<>();

		task.setId((long) 1);
		task2.setId((long) 2);
		tasks.add(task);
		tasks.add(task2);

		Mockito.when(taskRepository.findAll())
				.thenReturn(tasks);
		Mockito.when(taskRepository.save(task))
				.thenReturn(task);
		Mockito.when(taskRepository.findById(task2.getId()))
				.thenReturn(Optional.of(task));
	}

	@Test
	public void whenValidIdProvided_thenTaskShouldBeUpdated() {
		Task updatedTask = new Task(new Date(), "Updated", false);
		Task found = taskService.updateTask(updatedTask, (long) 2);

		assertEquals(updatedTask.getTaskName(), found.getTaskName());
	}

	@Test
	public void whenValidIdProvided_thenTaskShouldBeCompleted() {
		Task completedTask = taskService.completeTask((long) 2);

		assertTrue(completedTask.isCompleted());
	}

	@Test
	public void whenGettingTasks_thenTaskListShouldBeReturned() {
		List<Task> tasks = taskService.getAllTasks();

		assertEquals(tasks.get(0).getTaskName(), "First task");
	}
}
