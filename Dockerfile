FROM java:8
ADD target/erply-docker.jar erply-docker.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","erply-docker.jar"]