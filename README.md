# Tomas Kingissepp
Task API

#### Used technologies
- Java 1.8 
- Spring Boot 
- Swagger UI
- Maven wrapper
- Docker

## Run
### From Docker
Should be installed: 
```
JAVA 8 HOME(Win)
```
Go to root project directory: 
```
cd task-api
```
Clean & Package the project using the following command(Win): 
```
mvnw clean install
```
Build Docker image: 
```
docker build -t erply .
```
Run application:
```
docker run -p 8080:8080 -t erply
```
Open link in browser to see Swagger UI:
```
localhost:8080
```
### Directly from an IDE
#### Using main class
Run main class named `ErplyApplication` (package `com.application.erply`).

Open link in browser to see Swagger UI:
```
localhost:8080
```

## Description
API includes:

- Getting the list of tasks:
```
GET ../tasks
```
- Adding new task:
```
POST ../addTask    Arguments: Task object
```
- Replacing task:
```
PUT ../replaceTask/{id}    Arguments: Task object, Long taskId
```
- Completing task:
```
PUT ../completeTask/{id}    Arguments: Long taskId
``` 
- Deleting task:
```
DELETE ../deleteTask/{id}    Arguments: Long taskId
```
